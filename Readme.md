# Alkemy Blog Api Challenge

In this repo you will find the api for a frontend application called ("blog-client")

# HOW TO INSTALL

- Clone the repository
  `git clone https://veraManuel@bitbucket.org/veraManuel/blog.git`

- go to directory `cd blog `
- Run `npm install`
- Run `node index`

# LIBRARIES

- [Express](https://www.npmjs.com/package/express)
- [Cors](https://www.npmjs.com/package/cors)
- [Body-Parser](https://www.npmjs.com/package/body-parser)
- [Dotenv](https://www.npmjs.com/package/dotenv)
- [Mysql2](https://www.npmjs.com/package/mysql2)
- [Sequelize](https://www.npmjs.com/package/sequelize)
- [Image-url-Validator](https://www.npmjs.com/package/image-url-validator)
