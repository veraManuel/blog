const config = require("./");

module.exports = {
  user: config.mysql.user,
  password: config.mysql.password,
  host: config.mysql.host,
  database: config.mysql.database,
  dialect: config.mysql.dialect,
};
