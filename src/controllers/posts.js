const PostService = require("../services/post");
const postService = new PostService();

const createPost = async (req, res) => {
  await postService.createPost(req, res);
};

const getPosts = async (req, res) => {
  await postService.getPosts(req, res);
};

const getPostById = async (req, res) => {
  await postService.getPostById(req, res);
};

const updatePost = async (req, res) => {
  await postService.updatePost(req, res);
};

const deletePostById = async (req, res) => {
  await postService.deletePostById(req, res);
};

module.exports = {
  createPost,
  getPosts,
  getPostById,
  updatePost,
  deletePostById,
};
