const ExpressServer = require("./server/expressServer");

const config = require("../config");

module.exports = async () => {
  const server = new ExpressServer();

  server.start();
};
