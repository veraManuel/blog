const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const { sequelize } = require("../../models");
const config = require("../../config");

class ExpressServer {
  constructor() {
    this.app = express();
    this.port = config.port;
    this.basePathPost = `${config.api.prefix}/posts`;

    this._middelwares();

    this._cors();

    this._routes();

    this._notFound();
    this._errorHandler();
  }

  _middelwares() {
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
  }

  _cors() {
    const corsOptions = {
      origin: "*",
    };
    this.app.use(cors(corsOptions));
  }

  _routes() {
    this.app.use(this.basePathPost, require("../../routes/posts"));
  }

  _notFound() {
    this.app.use((req, res, next) => {
      const err = new Error("Not Found");
      err.code = 404;
      next(err);
    });
  }

  _errorHandler() {
    this.app.use((err, req, res, next) => {
      const code = err.code || 500;
      const body = {
        error: {
          code,
          message: err.message,
        },
      };
      res.status(code).json(body);
    });
  }

  async start() {
    this.app.listen(this.port, async () => {
      console.log(`Server up on port ${config.port}`);
      await sequelize.authenticate({ force: true });
      console.log("Database Conected");
    });
  }
}

module.exports = ExpressServer;
