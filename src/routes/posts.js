const { Router } = require("express");
const {
  createPost,
  getPosts,
  getPostById,
  updatePost,
  deletePostById,
} = require("../controllers/posts");

const router = Router();

router.post("/", createPost);
router.get("/", getPosts);
router.get("/:id", getPostById);
router.patch("/:id", updatePost);
router.delete("/:id", deletePostById);

module.exports = router;
