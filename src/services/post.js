const isImageURL = require("image-url-validator").default;
const { Post } = require("../models");

class PostService {
  createPost = async (req, res) => {
    const { title, content, image, category } = req.body;

    const validImage = await isImageURL(image);

    try {
      if (validImage || image === "") {
        const post = await Post.create({
          title,
          content,
          image,
          category,
        });

        return res.status(200).json(post);
      } else {
        return res
          .status(409)
          .send({ message: "The url is not from an image" });
      }
    } catch (error) {
      return res.status(400).send({ message: error });
    }
  };

  getPosts = async (req, res) => {
    await Post.findAll({
      attributes: { exclude: ["content", "updatedAt"] },
      order: [["createdAt", "DESC"]],
    })
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving the Posts",
        });
      });
  };

  getPostById = async (req, res) => {
    const { id } = req.params;
    await Post.findOne({
      where: { id },
    })
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving the Posts",
        });
      });
  };

  updatePost = async (req, res) => {
    const { id } = req.params;
    const { title, content, image, category } = req.body;

    const validImage = await isImageURL(image);

    try {
      const post = await Post.findOne({ where: { id } });

      if (validImage || image === "") {
        post.title = title;
        post.content = content;
        post.image = image;
        post.category = category;

        await post.save();

        return res.json(post);
      } else {
        return res
          .status(409)
          .send({ message: "The url is not from an image" });
      }
    } catch (error) {
      return res.status(400).send({ message: error });
    }
  };

  deletePostById = async (req, res) => {
    const { id } = req.params;

    try {
      const post = await Post.findOne({ where: { id } });

      await post.destroy();

      return res.status(200).send({ message: "Post deleted succcessfully" });
    } catch (error) {
      return res.status(400).send({ message: error });
    }
  };
}

module.exports = PostService;
